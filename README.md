# ***MOVIENLIGHT MOVIE REVIEW PLATFORM***
# This is only my personal repository for my mini-project on Glints Academy Batch #9.
# You can kindly check our Back-End Team project's repository on the link below:
# [`Click here for our Back-End Team repository!`](https://gitlab.com/glintsxbinar_miniproject_batch9/team-e/teame-backend)
# And also for our APIs documentation, you can kindly check on the link below:
# [`Click here for APIs documentation!`](https://documenter.getpostman.com/view/13708168/TVsx9kFD)